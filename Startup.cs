﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Remotion.Linq.Clauses;
using Serilog;

namespace MiddlewareTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(c =>
            {
                c.Filters.AddService<RequestLoggerFilter>(1);
                c.Filters.AddService<ModelStateValidationFilter>(2);
            }).AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddScoped<RequestLoggerFilter>();
            services.AddScoped<ModelStateValidationFilter>();

            services.AddOptions();
            services.Configure<AppOptions>(Configuration.GetSection("app"));

            var serviceProvider = services.BuildServiceProvider();
            serviceProvider.GetService<IOptions<AppOptions>>().Validate(serviceProvider.GetService<IValidator<AppOptions>>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler();
            app.UseExceptionLoggerHandler();

            app.UseMvc();
        }
    }

    public static class OptionsExtensions
    {
        public static void Validate<T>(this IOptions<T> options, IValidator<T> validator) where T : class, new()
        {
            if (!validator.Validate(options.Value).IsValid)
            {
                throw new ApplicationSettingsValidationException();
            }
        }
    }

    public class AppOptions
    {
        public string Name { get; set; }
    }

    public class AppOptionsValidator : AbstractValidator<AppOptions>
    {
        public AppOptionsValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name Wymagane");
        }
    }

    public class ApplicationSettingsValidationException : Exception
    {
    }

    public class RequestLoggerFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            Log.Logger.Information(string.Join("; ", context.HttpContext.Request.Path.Value, context.ActionArguments.Select(x => $"{x.Key} {x.Value.ToString()}")));
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            Log.Logger.Information(string.Join("; ", "End"));
        }
    }

    public class ModelStateValidationFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                throw new ModelStateValidationException(context.ModelState);
            }
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }

    public class ModelStateValidationException : Exception
    {
        public SerializableError Error { get; }

        public ModelStateValidationException(ModelStateDictionary modelState)
        {
            Error = new SerializableError(modelState);
        }
    }

    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;


        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var statusCode = HttpStatusCode.BadRequest;
            object result = null;

            var exceptionType = exception.GetType();
            switch (exception)
            {
                case Exception e when exceptionType == typeof(UnauthorizedAccessException):
                    statusCode = HttpStatusCode.Unauthorized;
                    break;
                case ModelStateValidationException e when exceptionType == typeof(ModelStateValidationException):
                    statusCode = HttpStatusCode.BadRequest;
                    result = e.Error;
                    break;
                case Exception e when exceptionType == typeof(Exception):
                    statusCode = HttpStatusCode.InternalServerError;
                    break;
            }

            var response = new ObjectResult(result);
            response.StatusCode = (int)statusCode;
            var payload = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;
            return context.Response.WriteAsync(payload);
        }
    }

    public class ExceptionLoggerHandlerMiddleware
    {
        private readonly RequestDelegate _next;


        public ExceptionLoggerHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                HandleException(context, ex);
                throw ex;
            }
        }

        private static void HandleException(HttpContext context, Exception exception)
        {
            Log.Logger.Error(exception, "Błąd");
        }
    }

    public static class Extensions
    {
        public static IApplicationBuilder UseExceptionHandler(this IApplicationBuilder builder)
            => builder.UseMiddleware(typeof(ExceptionHandlerMiddleware));
        public static IApplicationBuilder UseExceptionLoggerHandler(this IApplicationBuilder builder)
            => builder.UseMiddleware(typeof(ExceptionLoggerHandlerMiddleware));
    }
}
